using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SentryAI : MonoBehaviour
{
    public GameObject followMe;

    public bool detectPlayer;

    public float rotationSpeed;
    public float rotationDirection = 1;
    public bool equalAngles = false;
    public float angleA = -30;
    public float angleB = 30;
    public float sharedAngle;
    public float delayTime = 1;
    public bool ranOnce = true;

    public Vector3 offset = new Vector3( 0, -1, 0 );

    public GameObject projectilePrefab;
    public float fireRate = 2;
    private float counter = 0;

    // Start is called before the first frame update
    void Start()
    {
        /*
        float tempA = angleA - 180;
        tempA *= 0.5f;

        float tempB = angleB - 180;
        tempB *= 0.5f;

        angleA = 180 + tempA;
        angleB = 180 + tempB;
        */
        if(equalAngles == true)
        {
            angleA = 180 + -sharedAngle;
            angleB = 180 + sharedAngle;
        }
        
        rotationDirection = 1;
        transform.eulerAngles = new Vector3(0, 0, angleA);
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit2D hit = Physics2D.Raycast(transform.position + offset, transform.up);
        //Detect Player
        if (hit.collider.tag == "Player")
        {
            Debug.Log("Detected Player");
            detectPlayer = true;

            if(counter <= 0)
            {
                FireProjectile();
            }
        }
        else
        {
            detectPlayer = false;

        }

        //Draw line/laser
        Debug.DrawRay(transform.position + offset, transform.up * 30, Color.red, Time.deltaTime, false);

        //Fire projectile

        //Rotate sentry turret
        transform.Rotate(Vector3.forward * Time.deltaTime * rotationDirection * rotationSpeed);
        float rotationZ = (transform.eulerAngles.z);
        //Debug.Log(rotationZ.ToString());

        if (rotationZ >= angleB | rotationZ <= angleA)
        {
            if(ranOnce == true)
            {
                StartCoroutine(delayChangeRotationDirection());
                ranOnce = false;
            }
        }

        float difference = (angleB + angleA)/2;
        if(rotationZ < (difference + 5)&& rotationZ > (difference - 5))
        {
            ranOnce = true;
        }

        if(counter > 0)
        {
            counter -= Time.deltaTime;
        }
    }

    IEnumerator delayChangeRotationDirection()
    {
        float newDirection = rotationDirection * -1;
        rotationDirection = 0;
        yield return new WaitForSeconds(delayTime);

        rotationDirection = newDirection;
    }

    private void FireProjectile()
    {
        GameObject projectile = Instantiate(projectilePrefab,transform.position + offset, Quaternion.identity);
        
        Rigidbody2D rb = projectile.GetComponent<Rigidbody2D>();
        Projectile proj = projectile.GetComponent<Projectile>();
        if(rb != null)
        {
            rb.velocity = proj.speed * transform.up;
        }

        Destroy(projectile, 5f);

        counter = fireRate;
    }
}
