using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{
    public float speed;
    public int damage;
    public string[] damageTags;
    public string[] despawnTags;
    // Start is called before the first frame update

    private void OnTriggerEnter2D(Collider2D collision)
    {
        string collisionTag = collision.tag;
        if(CheckTag(damageTags, collisionTag))
        {
            Health hp = collision.GetComponent<Health>();
            if (hp != null)
            {
                hp.TakeDamage(damage);
            }
        }

        if(CheckTag(despawnTags, collisionTag))
        {
            Destroy(this.gameObject);
        }
    }

    private bool CheckTag(string[] tagCollection, string tagToCheck)
    {
        bool tagExists = false;
        for (int i = 0; i < tagCollection.Length; i++)
        {
            if(tagCollection[i] == tagToCheck)
            {
                tagExists = true;
            }
        }

        return tagExists;
    }
}
