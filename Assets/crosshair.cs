using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class crosshair : MonoBehaviour
{
    public int collidersTouched = 0;
    public bool isEmpty;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        isEmpty = false;
        Vector3 newPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        newPosition.z = 0;
        transform.position = newPosition;

        if(collidersTouched == 0)
        {
            isEmpty = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        collidersTouched += 1;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        collidersTouched -= 1;
    }
}
