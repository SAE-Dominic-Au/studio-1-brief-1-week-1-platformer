using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpikeTrap : MonoBehaviour
{
    public float speed;
    public Vector3 outPosition;
    public Vector3 idlePosition;
    public bool activated;
    public bool resetting;
    public float duration;
    public float counter;
    public int damage = 1;
    // Start is called before the first frame update
    void Start()
    {
        idlePosition = transform.position;
        outPosition = transform.position + new Vector3(0, 1, 0);
        this.GetComponent<BoxCollider2D>().size = this.GetComponent<SpriteRenderer>().size; //Safety check so the displayed image is the same size as the trigger box
    }

    private void Update()
    {
        if (activated)
        {
            Vector2 newSpeed = new Vector2(0, speed);

            if (transform.position.y >= outPosition.y)
            {
                newSpeed = Vector2.zero;
                transform.position = outPosition;
                activated = false;
                counter = duration;
            }
            this.GetComponent<Rigidbody2D>().velocity = newSpeed;
        }

        if (counter > 0)
        {
            counter -= Time.deltaTime;

            if(counter <=0)
            {
                resetting = true;
            }
        }

        if (resetting)
        {
            Vector2 newSpeed = new Vector2(0, -speed);
            if (transform.position.y <= idlePosition.y)
            {
                newSpeed = Vector2.zero;
                transform.position = idlePosition;
                resetting = false;
                counter = 0;
            }
            this.GetComponent<Rigidbody2D>().velocity = newSpeed;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            Health playerHealth = collision.GetComponent<Health>();
            if (playerHealth != null)
            {
                playerHealth.TakeDamage(damage);
            }
        }
    }
}
