using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapActivator : MonoBehaviour
{
    public SpikeTrap[] trapsToActivate;
    public float delay;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            StartCoroutine("delayedActivation");
        }
    }

    IEnumerator delayedActivation()
    {
        yield return new WaitForSeconds(delay);

        for (int i = 0; i < trapsToActivate.Length; i++)
        {
            trapsToActivate[i].activated = true;
            //trapsToActivate[i].counter = trapsToActivate.[i].duration; //reset to max duration every frame the player is still on the trigger
        }
    }
}
