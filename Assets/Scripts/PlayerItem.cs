using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerItem : MonoBehaviour
{
    public bool redKey = false;
    public bool blueKey = false;
    public bool yellowKey = false;

    private void Start()
    {
        ResetKeys();
    }

    void ResetKeys()
    {
        redKey = false;
        blueKey = false;
        yellowKey= false;
    }
}
