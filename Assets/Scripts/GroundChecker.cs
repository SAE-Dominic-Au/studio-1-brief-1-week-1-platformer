using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChecker : MonoBehaviour
{
    public bool onGround;

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Env")
        {
            onGround = false;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Env")
        {
            onGround = true;
        }
    }
}
