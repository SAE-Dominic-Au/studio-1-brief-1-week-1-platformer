using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public int SceneIndex;
    public int endSceneIndex;
    public float waitDelay;

    public static GameManager instance;


    // Start is called before the first frame update
    private void OnEnable()
    {
        EventManager.onPlayerDeathEvent += LevelRestart;
        EventManager.onLevelCompleteEvent += LevelComplete;
    }
    private void OnDisable()
    {
        EventManager.onPlayerDeathEvent -= LevelRestart;
        EventManager.onLevelCompleteEvent -= LevelComplete;
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if (instance != this)
            Destroy(gameObject);
    }

    private void Start()
    {
        SceneIndex = SceneManager.GetActiveScene().buildIndex;
        endSceneIndex = SceneManager.sceneCountInBuildSettings - 1;
    }
     void LevelComplete()
    {
        SceneIndex += 1;
        while (SceneIndex > endSceneIndex)
        {
            SceneIndex -= 1;
        }
        //SceneManager.LoadScene(SceneIndex, LoadSceneMode.Single);
        StartCoroutine(delayedLoadScene());
    }
     void LevelRestart()
    {
        //SceneManager.LoadScene(SceneIndex, LoadSceneMode.Single);
        StartCoroutine(delayedLoadScene());
    }

    IEnumerator delayedLoadScene()
    {
        yield return new WaitForSeconds(waitDelay);
        SceneManager.LoadScene(SceneIndex);
    }

}
