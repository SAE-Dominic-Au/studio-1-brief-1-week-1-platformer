using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slime : MonoBehaviour
{
    public string type;
    //Purple
    public float jumpModifier;
    //Blue
    public float speedModifier;
    //Green
    public float timeScaleModifier;
    private float counter;
    public float duration;

    private void Start()
    {
        if (type == "purple")
        {
            this.GetComponent<SpriteRenderer>().color = Color.magenta;
        }

        if (type == "blue")
        {
            this.GetComponent<SpriteRenderer>().color = Color.cyan;
        }

        if (type == "green")
        {
            this.GetComponent<SpriteRenderer>().color = Color.green;
        }
    }

    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            if(type == "purple")
            {
                PlayerMovement playerMovement = collision.GetComponent<PlayerMovement>();
                if (playerMovement != null)
                {
                    playerMovement.jumpModifier = jumpModifier;
                }
            }

            if (type == "blue")
            {
                PlayerMovement playerMovement = collision.GetComponent<PlayerMovement>();
                if (playerMovement != null)
                {
                    playerMovement.speedModifier = speedModifier;
                }
            }

            if (type == "green")
            {
                Time.timeScale = 0.5f;
                counter = duration;

                PlayerMovement playerMovement = collision.GetComponent<PlayerMovement>();
                if (playerMovement != null)
                {
                    playerMovement.timeScaleModifier = timeScaleModifier;
                }
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (type == "purple")
            {
                PlayerMovement playerMovement = collision.GetComponent<PlayerMovement>();
                if (playerMovement != null)
                {
                    playerMovement.jumpModifier = jumpModifier;
                }
            }

            if (type == "blue")
            {
                PlayerMovement playerMovement = collision.GetComponent<PlayerMovement>();
                if (playerMovement != null)
                {
                    playerMovement.speedModifier = speedModifier;
                }
            }

            if (type == "green")
            {
                counter = duration;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (type == "purple")
            {
                PlayerMovement playerMovement = collision.GetComponent<PlayerMovement>();
                if (playerMovement != null)
                {
                    playerMovement.jumpModifier = 1;
                }
            }

            if (type == "blue")
            {
                PlayerMovement playerMovement = collision.GetComponent<PlayerMovement>();
                if (playerMovement != null)
                {
                    playerMovement.speedModifier = 1;
                }
            }

            if (type == "green")
            {
                counter = duration;
            }
        }
    }

    private void Update()
    {
        if(counter > 0)
        {
            counter -= Time.deltaTime;
            if(counter <= 0)
            {
                Time.timeScale = 1;

                PlayerMovement playerMovement = FindObjectOfType<PlayerMovement>();
                if (playerMovement != null)
                {
                    playerMovement.timeScaleModifier = 1;
                }
            }
        }
    }
}
