using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyItem : MonoBehaviour
{
    public string type;

    private void Start()
    {
        if (type == "red")
        {
            this.GetComponent<SpriteRenderer>().color = Color.red;
        }
        if (type == "yellow")
        {
            this.GetComponent<SpriteRenderer>().color = Color.yellow;
        }
        if (type == "blue")
        {
            this.GetComponent<SpriteRenderer>().color = Color.blue;
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            PlayerItem playerItem = collision.GetComponent<PlayerItem>();
            if(playerItem != null)
            {
                if(type == "red")
                {
                    playerItem.redKey = true;
                    
                }
                if (type == "yellow")
                {
                    playerItem.yellowKey = true;
                }
                if (type == "blue")
                {
                    playerItem.blueKey = true;
                }
            }

            EventManager.onKeyChangeEvent?.Invoke();

            Destroy(this.gameObject);
            //OR
            //gameObject.SetActive(false);
        }

    }
}
