using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float horizontalVelocity = 5;
    public float jumpForce = 8;

    public GroundChecker onGroundTester;

    public float jumpModifier = 1;
    public float speedModifier = 1;
    public float timeScaleModifier = 1;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A)) //press A to move left
        {
            float speed = horizontalVelocity * speedModifier * timeScaleModifier;
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            float speed = horizontalVelocity * speedModifier * timeScaleModifier;

            this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, this.GetComponent<Rigidbody2D>().velocity.y);
        }
        else
        {
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(0, this.GetComponent<Rigidbody2D>().velocity.y);
        }

        if (Input.GetKey(KeyCode.Space) && onGroundTester.onGround == true)
        {
            float jump = jumpForce * jumpModifier * timeScaleModifier;
            this.GetComponent<Rigidbody2D>().velocity = new Vector2(this.GetComponent<Rigidbody2D>().velocity.x, jump);
        }
    }
}
