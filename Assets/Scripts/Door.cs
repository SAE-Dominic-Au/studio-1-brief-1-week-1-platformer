using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
    public bool redKey = true;
    public bool yellowKey = true;
    public bool blueKey = true;

    // Start is called before the first frame update
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "Player")
        {
            PlayerItem PI = collision.GetComponent<PlayerItem>();
            if(PI != null)
            {
                if ((redKey == PI.redKey) && (yellowKey == PI.yellowKey) && (blueKey == PI.blueKey)) 
                {
                    EventManager.onLevelCompleteEvent?.Invoke();
                    Debug.Log("Level Completed");
                }

            }
        }
    }
}
