using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class EventManager
{
    public delegate void OnHealthChange();
    public delegate void OnKeyChange();
    public delegate void OnPlayerDeath();
    public delegate void OnLevelComplete();

    public static OnHealthChange onHealthChangeEvent;
    public static OnKeyChange onKeyChangeEvent;
    public static OnPlayerDeath onPlayerDeathEvent;
    public static OnLevelComplete onLevelCompleteEvent;
}