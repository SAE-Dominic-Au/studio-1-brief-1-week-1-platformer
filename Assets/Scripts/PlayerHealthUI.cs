using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthUI : MonoBehaviour
{
    public Health playerHealth;

    private void OnEnable()
    {
        EventManager.onHealthChangeEvent += UpdateHeatlhUI;
    }

    private void OnDisable()
    {
        EventManager.onHealthChangeEvent -= UpdateHeatlhUI;
    }

    void Start()
    {
        UpdateHeatlhUI();
    }

    // Update is called once per frame
    void UpdateHeatlhUI()
    {
        for(int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).GetComponent<Image>().color = Color.grey;
        }

        for (int i = 0; i < playerHealth.currentHP; i++)
        {
            transform.GetChild(i).GetComponent<Image>().color = Color.red;
        }
    }
}
