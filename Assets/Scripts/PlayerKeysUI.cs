using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerKeysUI : MonoBehaviour
{
    public PlayerItem playerItem;

    private void OnEnable()
    {
        EventManager.onKeyChangeEvent += UpdateKeyUI;
    }

    private void OnDisable()
    {
        EventManager.onKeyChangeEvent -= UpdateKeyUI;
    }

    void Start()
    {
        UpdateKeyUI();
    }

    // Update is called once per frame
    void UpdateKeyUI()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).GetComponent<Image>().color = Color.grey;
        }

        if(playerItem.redKey == true)
        {
            transform.GetChild(0).GetComponent<Image>().color = Color.red;
        }

        if (playerItem.yellowKey == true)
        {
            transform.GetChild(1).GetComponent<Image>().color = Color.yellow;
        }

        if (playerItem.blueKey == true)
        {
            transform.GetChild(2).GetComponent<Image>().color = Color.blue;
        }
    }
}
