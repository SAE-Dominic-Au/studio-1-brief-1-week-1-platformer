using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health : MonoBehaviour
{
    public int currentHP = 3;
    public int maxHP = 3;

    public bool player;
    /*
    private void OnEnable()
    {
        EventManager.onPlayerDeathEvent += Death;
    }
    private void OnDisable()
    {
        EventManager.onPlayerDeathEvent -= Death;
    }
    */
    // Start is called before the first frame update

    void Start()
    {
        currentHP = maxHP;
    }

    public void TakeDamage(int DamageTaken)
    {
        currentHP -= DamageTaken;

        EventManager.onHealthChangeEvent?.Invoke();

        if(currentHP <= 0)
        {
            Death();
        }
    }

    void Death()
    {
        Destroy(this.gameObject);
        if (player == true)
        {
            EventManager.onPlayerDeathEvent?.Invoke();
        }
    }
}
